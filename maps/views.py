from django.shortcuts import render
from django.http import HttpResponse
from config.settings import MAPBOX_TOKEN
from maps.models import Location

def index(request):
    from json import dumps
    locations = Location.objects.all()
    location_list = [l.serialize() for l in locations]
    location_dict = {
        "type": "FeatureCollection",
        "features": location_list
    }
    location_json = dumps(location_dict)

    context = {
        'MAPBOX_TOKEN': MAPBOX_TOKEN,
        'locations': location_json,
    }
    return render(request, 'index.html', context)